# CRUD-admin



## Getting started

- Download Nodejs following this link : https://nodejs.org/en/

- run 
    ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```
in home terminal to install HomeBrew

- Next; run 
    ```brew install nodejs```

- Verification that node and npm versions are  "Node >= 14.0.0 and npm >= 5.6", by running 
    ```node -v```
    ```npm -v```

- Once, all this is verified, we can create our React app by running : 
    ```npx create-react-app admin-crud```

- Install "react-router-dom" in order to implement dynamic routing in a web app :
    ```npm install react-router-dom```

- Install axios to make it easy to send asynchronous HTTP requests to REST endpoints and perform CRUD operations : 
    ```npm install axios```
